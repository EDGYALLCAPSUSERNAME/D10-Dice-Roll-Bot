import re
import time
import praw
import random
import sqlite3
import OAuth2Util


# User config
# -----------------------------------------------------------------
# the name of the subreddit you want it to post in
# don't include the /r/
SUBREDDIT_NAME = ''

# the string you want it to find for it to reply
IDENTIFIER = '!d10'

# -----------------------------------------------------------------


def reply_to_comment(comment, reply):
    print('Replying to comment...')
    comment.reply(reply)


def build_comment(pool, modifiers):
    # Build the first line of comment
    if pool != 'c':
        comment = 'Rolling a dice pool of {}'.format(pool)
        if modifiers:
            if modifiers == '-ex9':
                comment += ' on 9-Again rule'
    else:
        comment = 'Rolling a chance die'

    comment += ':\n\n'

    # Do the initial role
    results = roll_dice(pool, modifiers)
    comment += results['results'] + '\n\n'

    # store the nines
    nines = results['9-again?']

    while True:
        # Loop to roll 10s until no more
        while results['10-again?']:
            results = roll_dice(results['10-again?'], None)
            comment += '10-Again!\n\n'
            comment += results['results'] + '\n\n'

        # Loop to roll 9s until no more
        while nines:
            results = roll_dice(nines, '-ex9')
            nines = results['9-again?']
            comment += '9-Again!\n\n'
            comment += results['results'] + '\n\n'

        if not results['10-again?'] and not nines:
            break

    return comment

def roll_dice(pool, modifiers=None):
    print('Rolling dice...')
    dice_rolls = []
    if pool == 'c':
        roll = random.randrange(1, 11)
        reply = '({})'.format(roll)
        if roll == 1:
            reply += '\n\n\n\n DRAMATIC FAILURE'
        elif roll == 10:
            reply += '\n\n\n\n Success!'

        return {'results': reply,
                '10-again?': 0,
                '9-again?': 0}
    else:
        # roll dice n number of times
        for i in range(0, int(pool)):
            # add a random number in the range of 1-10
            dice_rolls.append(random.randrange(1, 11))

    # find if/how many 10s were rolled
    tens = 0
    for n in dice_rolls:
        if n == 10:
            tens += 1

    nines = 0
    # check if we need to do the same for 9s
    if modifiers == '-ex9':
        for n in dice_rolls:
            if n == 9:
                nines += 1

    if len(dice_rolls) > 1:
        dice_results = '(' + ', '.join(str(i) for i in dice_rolls) + ')'
    else:
        dice_results = '(' + str(dice_rolls[0]) + ')'

    return {'results': dice_results,
            '10-again?': tens,
            '9-again?': nines}

def examine_comment(r, comment):
    # break the comment into a list
    commands = str(comment).replace('\n', ' ').split(' ')
    # get rid of blank strings
    commands = [x for x in commands if x]
    # get rid of anything before it
    commands = commands[commands.index('!d10'):]
    # try to get modifiers if they exist
    try:
        if commands[2] == '-ex9':
            modifiers = commands[2]
        else:
            modifiers = None
    except IndexError:
        modifiers = None

    reply = build_comment(commands[1], modifiers)

    reply_to_comment(comment, reply)


def scan_comments(r, o, cur, sql):
    # get a stream of all comments for the subreddit
    comment_stream = praw.helpers.comment_stream(r, SUBREDDIT_NAME)
    for comment in comment_stream:
        # check the comment body for the identifier
        if re.search(IDENTIFIER, str(comment), re.IGNORECASE):
            # check to make sure the bot hasn't already replied
            cur.execute('SELECT ID FROM replied WHERE ID = ?', [comment.id])
            if cur.fetchone() == None:
                print('Found a comment...')
                examine_comment(r, comment)

        # add the comment to the database to keep it from replying
        # multiple times to the same comment
        cur.execute('INSERT INTO replied VALUES(?)', [comment.id])
        # and commmit the change to the db
        sql.commit()


def main():
    r = praw.Reddit(user_agent='D10_Dice_Roller v2.0 /u/EDGYALLCAPSUSERNAME')
    o = OAuth2Util.OAuth2Util(r, print_log=True)

    # Setup/Load the database
    sql = sqlite3.connect('d10replied.db')
    cur = sql.cursor()
    cur.execute('CREATE TABLE IF NOT EXISTS replied(ID TEXT)')
    print('Loaded SQL Database')
    sql.commit()

    while True:
        try:
            scan_comments(r, o, cur, sql)
        except Exception as e:
            print('ERROR: {}'.format(e))
            print('Sleeping...')
            time.sleep(500)


if __name__ == '__main__':
    main()
